In order to show off your creativity and coding skills; please can you create a web page containing 
a tool to record basic information about an incident that has occurred somewhere in the city. 
The reporter must be able to record at least the following information about the incident:
·       A summary or title
·       Extended details
·       Date & time it happened
·       Precise location where it happened
We’ve been non-specific about the details; we’re interested in how you choose to do it as much as the result. 
It’s a simple task so don’t spend many hours on it. Write something tiny and perhaps a couple of sentences about your thinking.


To run the app unzip and go inside itr.jobtests.apdnorthgate folderopen the folder using visual studio code and use the following commands.
npm install followed by npm start.
to run teests use npm test.


Comments and EnhancementsI have created a tool with multiple pages for adding, viewing, listing and editing the incident.
I have added very basic validation but more specific validation can be added.
I have used a context to pass the context list down to the child components for viewing, adding, editing and deleting incident
For proper use, we should store the incidents on the back end database and retrieve them from the db.
List component should use paging and only load specific pages otherwise it will be very slow for long lists.
I have added 2 snapshot tests but more should be added for shallow as well as dom testing.
We can use Redux for state management when backend is active to avoid prop drilling and easy unit testing.
