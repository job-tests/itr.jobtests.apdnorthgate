import React, { Component } from 'react';
import './App.css';
import { Route, Switch, Redirect } from 'react-router-dom';
import { IIncident } from './components/Incident/useIncident';
import { IncidentListContext } from './context/IncidentListContext';
import IncidentList from './components/IncidentList/IncidentList';
import AddIncident from './components/AddIncident/AddIncident';
import ApdAppBar from './components/AppBar/ApdAppBar';
import { ViewIncident } from './components/ViewIncident/ViewIncident';
import PageNotFound from './components/PageNotFound/PageNotFound';

const list = [] as IIncident[];

class App extends Component {
  render() {
    return (
      <>
        <IncidentListContext.Provider value={list}>
          <ApdAppBar></ApdAppBar>
          <Switch>
            <Route path='/incidents' exact component={IncidentList}></Route>
            <Route path='/incidents/add' exact component={AddIncident}></Route>
            <Route exact path='/incidents/:incidentId/edit' component={AddIncident}></Route>
            <Route exact path='/incidents/:incidentId/view' component={ViewIncident}></Route>
            <Redirect exact from='/' to='/incidents'></Redirect>
            <Route exact path='/pagenotfound' component={PageNotFound}></Route>
            <Redirect to='/pagenotfound'></Redirect>
          </Switch>
        </IncidentListContext.Provider>
      </>
    );
  }
}

export default App;
