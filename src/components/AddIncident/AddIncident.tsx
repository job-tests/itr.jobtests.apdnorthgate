import React from 'react';
import Container from '@material-ui/core/Container';
import {
  CssBaseline,
  Grid,
  Button,
  Avatar,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import useStyles from './useStyles';
import { RouteComponentProps } from 'react-router-dom';
import AddIncidentIcon from '@material-ui/icons/AddAlert';
import UpdateIncidentIcon from '@material-ui/icons/Update';
import CancelIcon from '@material-ui/icons/Cancel';
import useAddIncident, { IncidentRouteParam } from './useAddIncident';
import Incident from '../Incident/Incident';
const AddIncident = (props: RouteComponentProps<IncidentRouteParam>) => {
  const classes = useStyles({});
  const {
    incident,
    handleChange,
    handleSubmit,
    error,
    openOrCloseDialog,
    openCancelDialog,
  } = useAddIncident(props);

  return (
    <>
      <CssBaseline />
      <Container component='main' maxWidth='md'>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <AddIncidentIcon />
          </Avatar>
          <Typography component='h1' variant='h5'>
            {incident.id ? 'Edit incident' : 'Add new incident'}
          </Typography>
        </div>
      </Container>
      <Container>
        <form className={classes.form} onSubmit={handleSubmit} noValidate>
          <Incident {...{ incident, handleChange, error }}></Incident>

          <Grid container justify='center' alignItems='center'>
            <Grid item xs={12} md={6}>
              <Button
                type='submit'
                variant='contained'
                color='primary'
                startIcon={incident.id ? <UpdateIncidentIcon /> : <AddIncidentIcon />}
                className={classes.submit}>
                {incident.id ? 'Update incident' : 'Add incident'}
              </Button>
              <Button
                type='button'
                variant='outlined'
                color='primary'
                startIcon={<CancelIcon />}
                onClick={(e) => openOrCloseDialog(true)}
                className={classes.cancel}>
                Cancel
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
      <Dialog
        open={openCancelDialog}
        onClose={() => openOrCloseDialog(false)}
        aria-labelledby='form-dialog-title'>
        <DialogTitle id='form-dialog-title'>Confirm cancellation</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to cancel? You will lose your unsaved changes.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => props.history.push('/incidents')} color='primary'>
            Confirm
          </Button>
          <Button onClick={() => openOrCloseDialog(false)} color='primary'>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default AddIncident;
