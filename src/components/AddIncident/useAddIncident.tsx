import { FormEventHandler, useContext, useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import useIncident, { IIncident } from '../Incident/useIncident';
import { IncidentListContext } from '../../context/IncidentListContext';

export interface IAddOrUpdateIncident {
  incidentProps: IIncident;
  routeProps: RouteComponentProps<any>;
}

export type IncidentRouteParam = {
  incidentId: string;
};

const InitIncident: IIncident = {
  summary: '',
  details: '',
  id: 0,
  incidentDate: '',
  incidentLocation: '',
} as IIncident;
const useAddIncident = ({
  match: {
    params: { incidentId },
  },
  history,
}: RouteComponentProps<IncidentRouteParam>) => {
  const context = useContext(IncidentListContext);

  const [state, setstate] = useState<IIncident>(InitIncident);
  const [openCancelDialog, setOpenCancelDialog] = useState<boolean>(false);

  const openOrCloseDialog = (open: boolean) => {
    setOpenCancelDialog(open);
  };

  useEffect(() => {
    const id = isNaN(Number(incidentId)) ? 0 : Number(incidentId);
    const incidentToEdit = context.find((c) => c.id === id);
    if (id && (!context || context.length === 0 || !incidentToEdit)) {
      history.push('/incidents/add');
      return;
    }

    const incidentProp = id ? incidentToEdit : InitIncident;

    setstate(incidentProp);
  }, [context, history, incidentId]);

  const { incident, handleChange, error, isIncidentValid } = useIncident(state);

  const handleSubmit: FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    if (isIncidentValid()) {
      addIncident();
    }
  };

  const addIncident = () => {
    const id =
      state.id > 0
        ? state.id
        : context && context.length > 0
        ? Math.max(...context.map((m) => m.id)) + 1
        : 1;
    const newIncident = { ...incident, id, dateCreated: new Date(), dateModified: new Date() };
    if (state.id) {
      const index = context.findIndex((c) => c.id === state.id);
      context.splice(index, 1);
      console.log(context);
    }
    context.push(newIncident);
    context.sort((a, b) => a.id - b.id);
    history.push('/incidents');
  };

  return { incident, handleChange, handleSubmit, error, openOrCloseDialog, openCancelDialog };
};

export default useAddIncident;
