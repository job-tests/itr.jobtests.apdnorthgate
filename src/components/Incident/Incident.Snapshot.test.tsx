import React from 'react';
import Incident from './Incident';

import renderer from 'react-test-renderer';
import { IIncidentProps, IIncident } from './useIncident';

const incidentProps: IIncidentProps = {
  incident: {
    summary: '',
    details: '',
    id: 0,
    incidentDate: '',
    incidentLocation: '',
  } as IIncident,
  error: { summary: '', details: '', incidentDate: '', incidentLocation: '' },
  handleChange: jest.fn(),
};

const editIncidentProps: IIncidentProps = {
  incident: {
    summary: '',
    details: '',
    id: 1,
    incidentDate: '',
    incidentLocation: '',
  } as IIncident,
  error: { summary: '', details: '', incidentDate: '', incidentLocation: '' },
  handleChange: jest.fn(),
};

it('should set correct button label', () => {
  const incident = renderer.create(<Incident {...incidentProps}></Incident>);
  expect(incident).toMatchSnapshot();
});

it('should set correct button label', () => {
  const incident = renderer.create(<Incident {...editIncidentProps}></Incident>);
  expect(incident).toMatchSnapshot();
});
