import React from 'react';
import { TextField, Grid } from '@material-ui/core';
import { IIncidentProps } from './useIncident';

const Incident = ({
  incident: { summary, details, incidentLocation, incidentDate },
  error: {
    summary: summaryError,
    details: detailsError,
    incidentLocation: incidentLocationError,
    incidentDate: incidentDateError,
  },
  handleChange,
}: IIncidentProps) => (
  <React.Fragment>
    <Grid container justify='center' alignItems='center' spacing={2}>
      <Grid item xs={12} md={6}>
        <TextField
          autoFocus
          error={!!summaryError}
          placeholder='Incident title'
          autoComplete='Incident title'
          name='summary'
          variant='outlined'
          required
          fullWidth
          id='summary'
          value={summary}
          onChange={handleChange}
          label='Incident title'
          helperText={summaryError}
        />
      </Grid>
    </Grid>
    <Grid container justify='center' alignItems='center' spacing={2}>
      <Grid item xs={12} md={6}>
        <TextField
          error={!!incidentLocationError}
          placeholder='Incident location'
          autoComplete='Incident location'
          name='incidentLocation'
          variant='outlined'
          required
          fullWidth
          id='incidentLocation'
          value={incidentLocation}
          onChange={handleChange}
          label='Incident location'
          helperText={incidentLocationError}
        />
      </Grid>
    </Grid>
    <Grid container justify='center' alignItems='center' spacing={2}>
      <Grid item xs={12} md={6}>
        <TextField
          error={!!incidentDateError}
          placeholder='Incident date'
          autoComplete='Incident date'
          name='incidentDate'
          variant='outlined'
          required
          fullWidth
          id='incidentDate'
          value={incidentDate}
          onChange={handleChange}
          label='Incident date'
          helperText={incidentDateError}
        />
      </Grid>
    </Grid>
    <Grid container justify='center' alignItems='center' spacing={2}>
      <Grid item xs={12} md={6}>
        <TextField
          error={!!detailsError}
          placeholder='Incident details'
          autoComplete='Incident details'
          multiline
          rows={10}
          name='details'
          variant='outlined'
          required
          fullWidth
          id='details'
          value={details}
          onChange={handleChange}
          label='Incident details'
          helperText={detailsError}
        />
      </Grid>
    </Grid>
  </React.Fragment>
);

export default Incident;
