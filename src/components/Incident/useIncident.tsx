import { ChangeEventHandler, useState, useEffect } from 'react';
import moment from 'moment';

export interface IIncidentError {
  summary: string;
  details: string;
  incidentLocation: string;
  incidentDate: Date | string;
}

export interface IIncident extends IIncidentError {
  id: number;
  dateCreated: Date;
  dateModified: Date;
}

export interface IIncidentProps {
  incident: IIncident;
  error: IIncidentError;
  handleChange: ChangeEventHandler<HTMLInputElement>;
}

const useIncident = (incidentProp: IIncident) => {
  const [incident, setIncident] = useState<IIncident>({
    details: '',
    summary: '',
    incidentDate: '',
    incidentLocation: '',
    id: 0,
  } as IIncident);

  useEffect(() => {
    setIncident(incidentProp);
  }, [incidentProp]);

  const [error, setError] = useState<IIncidentError>({
    incidentDate: '',
    incidentLocation: '',
    summary: '',
    details: '',
  });

  const handleChange: ChangeEventHandler<HTMLInputElement> = ({ target: { name, value } }) => {
    setIncident({ ...incident, [name]: value });

    const errorMessage = !value ? `${name} is required` : '';
    setError({ ...error, [name]: errorMessage });
  };

  const isIncidentValid = () => {
    const iDate = moment(incident.incidentDate, 'DD/MMM/YYYY');
    const valid =
      incident.details && incident.summary && incident.incidentLocation && iDate.isValid();

    setError({
      summary: incident.summary ? '' : `Name is required`,
      details: incident.details ? '' : 'Email is required',
      incidentLocation: incident.incidentLocation ? '' : 'Incident location is required',
      incidentDate: iDate.isValid() ? '' : 'A valid incident date is required',
    });

    return valid;
  };

  return { incident, error, handleChange, isIncidentValid };
};

export default useIncident;
