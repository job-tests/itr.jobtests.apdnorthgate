import React from 'react';
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Container,
  CssBaseline,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';

import moment from 'moment';

import DeleteIcon from '@material-ui/icons/Delete';
import { RouteComponentProps } from 'react-router-dom';
import EditIncidentIcon from '@material-ui/icons/Edit';
import AddIncidentIcon from '@material-ui/icons/AddAlert';
import VisibilityIcon from '@material-ui/icons/Visibility';
import useIncidentList from './useIncidentList';

const IncidentList = (props: RouteComponentProps) => {
  const {
    message,
    classes,
    openOrCloseDialog,
    deleteId,
    openDialog,
    handleDelete,
    handleEdit,
    showIncidents,
    state,
    history,
  } = useIncidentList(props);

  console.log(state);
  return (
    <>
      <CssBaseline />
      <h2>{message}</h2>
      <Container>
        {showIncidents === true && (
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label='simple table'>
              <TableHead>
                <TableRow>
                  <TableCell align='left'>Title</TableCell>
                  <TableCell align='left'>Incident date</TableCell>
                  <TableCell align='left'>Date created</TableCell>
                  <TableCell align='left'>Date modified</TableCell>
                  <TableCell align='left'>View</TableCell>
                  <TableCell align='left'>Edit</TableCell>
                  <TableCell align='left'>Delete</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {state.map((incident) => (
                  <TableRow key={incident.id}>
                    <TableCell align='left'>{incident.summary}</TableCell>
                    <TableCell align='left'>
                      {moment(incident.incidentDate, 'DD/MM/YYYY').format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell align='left'>
                      {moment(incident.dateCreated).format('DD/MM/YYYY HH:mm')}
                    </TableCell>
                    <TableCell align='left'>
                      {moment(incident.dateModified).format('DD/MM/YYYY HH:mm')}
                    </TableCell>
                    <TableCell align='left'>
                      <Button
                        variant='contained'
                        color='primary'
                        className={classes.button}
                        onClick={() => history.push(`incidents/${incident.id}/view`)}
                        startIcon={<VisibilityIcon />}>
                        View
                      </Button>
                    </TableCell>
                    <TableCell align='left'>
                      <Button
                        variant='contained'
                        color='primary'
                        className={classes.button}
                        onClick={() => handleEdit(incident.id)}
                        startIcon={<EditIncidentIcon />}>
                        Edit
                      </Button>
                    </TableCell>
                    <TableCell align='left'>
                      <Button
                        variant='contained'
                        color='secondary'
                        className={classes.button}
                        onClick={() => openOrCloseDialog(true, incident.id)}
                        startIcon={<DeleteIcon />}>
                        Delete
                      </Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        )}
      </Container>
      <br></br>
      <br></br>
      <Button
        variant='contained'
        color='primary'
        className={classes.button}
        onClick={() => {
          history.push('/incidents/add');
        }}
        startIcon={<AddIncidentIcon />}>
        Add new incident
      </Button>

      <Dialog
        open={openDialog}
        onClose={() => openOrCloseDialog(false)}
        aria-labelledby='form-dialog-title'>
        <DialogTitle id='form-dialog-title'>Confirm delete</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete the incident permanantely?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleDelete(deleteId)} color='primary'>
            Confirm
          </Button>
          <Button onClick={() => openOrCloseDialog(false)} color='primary'>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default IncidentList;
