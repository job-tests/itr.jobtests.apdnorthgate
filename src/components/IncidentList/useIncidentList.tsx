import { RouteComponentProps } from 'react-router-dom';
import { useContext, useState, useEffect } from 'react';
import { IIncident } from '../Incident/useIncident';
import { IncidentListContext } from '../../context/IncidentListContext';
import useStyles from './useStyles';

const useIncidentList = ({ history }: RouteComponentProps) => {
  const context = useContext<IIncident[]>(IncidentListContext);

  const [state, setState] = useState<IIncident[]>([]);

  useEffect(() => {
    setState([...context]);
  }, [context]);

  const handleEdit = (id: number) => {
    history.push(`incidents/${id}/edit`);
  };

  const handleDelete = (id: number) => {
    setOpenDialog(false);
    const index = context.findIndex((m) => m.id === id);
    if (id !== -1) {
      context.splice(index, 1);
      setState([...context]);
      setDeleteId(null);
    }
  };

  const [openDialog, setOpenDialog] = useState<boolean>(false);
  const [deleteId, setDeleteId] = useState<number>(null);

  const openOrCloseDialog = (open: boolean, incidentId?: number) => {
    if (incidentId) {
      setDeleteId(incidentId);
    }
    setOpenDialog(open);
  };

  const classes = useStyles({});
  const showIncidents = state && state.length && state.length > 0;
  const message = !showIncidents
    ? 'Incident list is empty please add incidents'
    : 'Incident list';

  return {
    message,
    classes,
    openOrCloseDialog,
    deleteId,
    openDialog,
    handleDelete,
    handleEdit,
    showIncidents,
    state,
    history,
  };
};

export default useIncidentList;
