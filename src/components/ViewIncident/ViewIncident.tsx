import React, { useEffect, useState } from 'react';
import { IIncident } from '../Incident/useIncident';
import Container from '@material-ui/core/Container';
import {
  Grid,
  Card,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Button,
  makeStyles,
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import EditIcon from '@material-ui/icons/Edit';
import { RouteComponentProps } from 'react-router-dom';
import { IncidentRouteParam } from '../AddIncident/useAddIncident';
import { useContext } from 'react';
import moment from 'moment';
import { IncidentListContext } from '../../context/IncidentListContext';

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
  cancel: {
    margin: theme.spacing(3, 0, 2),
    marginLeft: '20px !important',
  },
}));

export const ViewIncident = ({ history, match }: RouteComponentProps<IncidentRouteParam>) => {
  const classes = useStyles({});

  const incidentList: IIncident[] = useContext(IncidentListContext);

  const [incident, setIncident] = useState<IIncident>({
    summary: '',
    details: '',
    incidentLocation: '',
    incidentDate: '',
    dateCreated: null,
    dateModified: null,
    id: 0,
  });

  useEffect(() => {
    if (match && match.params && match.params.incidentId) {
      if (incidentList && incidentList.length === 0) {
        history.push('/incidents');
      } else {
        const incident = incidentList.find((m) => m.id === +match.params.incidentId);
        setIncident(incident);
      }
    }
  }, [incidentList, history, match]);

  return (
    <Container className={classes.cardGrid} maxWidth='md'>
      {/* End hero unit */}
      <Grid container spacing={4}>
        <Grid item xs={12} sm={6}>
          <Card className={classes.card}>
            <CardMedia
              className={classes.cardMedia}
              image='https://source.unsplash.com/random'
              title='Image title'
            />
            <CardContent className={classes.cardContent}>
              <Typography gutterBottom variant='h5' component='h2'>
                Title: {incident.summary}
              </Typography>
              <Typography gutterBottom variant='h5' component='h2'>
                Location: {incident.incidentLocation}
              </Typography>
              <Typography>
                Incident Date:{' '}
                {moment(incident.incidentDate, 'DD/MM/YYYY').format('DD/MM/YYYY')}
              </Typography>
              <Typography>Details: {incident.details}</Typography>
              <Typography>
                Date created: {moment(incident.dateCreated).format('DD/MM/YYYY HH:mm')}
              </Typography>
              <Typography>
                Date modified: {moment(incident.dateModified).format('DD/MM/YYYY HH:mm')}
              </Typography>
            </CardContent>
            <CardActions>
              <Button
                variant='contained'
                color='primary'
                startIcon={<EditIcon />}
                onClick={(e) => history.push(`/incidents/${incident.id}/edit`)}
                className={classes.submit}>
                Edit
              </Button>
              <Button
                type='button'
                variant='outlined'
                color='primary'
                startIcon={<ArrowBackIcon />}
                onClick={(e) => history.push(`/incidents`)}
                className={classes.cancel}>
                Back
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};
