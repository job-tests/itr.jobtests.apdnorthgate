import { createContext } from 'react';
import { IIncident } from '../components/Incident/useIncident';

export const IncidentListContext = createContext<IIncident[]>([]);
